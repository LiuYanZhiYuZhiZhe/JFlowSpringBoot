package BP.Port;

import BP.DA.*;
import BP.En.*;
import BP.Web.*;
import BP.Sys.*;
import java.util.*;

/** 
 部门属性
*/
public class DeptAttr extends EntityNoNameAttr
{
	/** 
	 父节点的编号
	*/
	public static final String ParentNo = "ParentNo";
	public static final String Idx = "Idx";
	public static final String OrgNo = "OrgNo";
}